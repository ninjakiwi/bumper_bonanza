use std::f32::consts::PI;

use bevy::{input::{keyboard::KeyCode, Input}, prelude::*, window::WindowResized};
use bevy_prototype_lyon::prelude::*;
use bevy_rapier2d::{na::{Isometry2, UnitVector2, Vector2}, physics::{ColliderHandleComponent, EventQueue, RapierConfiguration, RapierPhysicsPlugin, RigidBodyHandleComponent}, rapier::{dynamics::{RigidBodyBuilder, RigidBodySet}, geometry::ColliderBuilder}};

pub const PLAYER_FORCE: f32 = 1300000.0;
pub const PLAYER_FRICTION: f32 = 0.0;
pub const PLAYER_RESTITUTION: f32 = 0.5;
pub const PLAYER_RADIUS: f32 = 20.0;
pub const PLAYER_DAMPING: f32 = 0.9;

pub const PUCK_FRICTION: f32 = 0.0;
pub const PUCK_RESTITUTION: f32 = 0.5;
pub const PUCK_RADIUS: f32 = 10.0;
pub const PUCK_DAMPING: f32 = 0.1;

pub const GOAL_WIDTH: f32 = 80.0;
pub const GOAL_DEPTH: f32 = 60.0;

pub const WALL_FRICTION: f32 = 0.0;
pub const WALL_RESTITUTION: f32 = 1.0;

pub const TEAM_COLOURS: & [Color] = &[Color::rgb(0.02, 0.6, 1.0), Color::rgb(1.0, 0.025, 0.03)];

struct WorldBorder;
struct Team;
struct Player;
// Where Entity is a Team
struct Goal {
    associated_team: Entity
}
struct Puck;
struct ScoreText;
struct StartPosition((f32, f32));

struct Score(u32);

struct ControlForce(f32);

//events
struct Scored(Entity);
struct RestartGame;

struct Controls {
    up: KeyCode,
    down: KeyCode,
    left: KeyCode,
    right: KeyCode,
}

struct ControlsState(Vector2<f32>);

#[derive(Bundle)]
struct TeamBundle {
    team: Team,
    score: Score,
}

#[derive(Bundle)]
struct GoalBundle {
    goal: Goal,
    transform: Transform,
}

#[derive(Bundle)]
struct PlayerBundle {
    player: Player,
    control_force: ControlForce,
    controls: Controls,
    controls_state: ControlsState,
}


fn main() {
    App::build()
        .insert_resource(Msaa { samples: 8 })
        .add_plugin(GamePlugin)
        .run();
}



pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut AppBuilder) {
        // true to repeat timer
        app
            .insert_resource(ClearColor(Color::rgb(1.0, 1.0, 1.0)))
            .add_plugins(DefaultPlugins)
            .add_plugin(RapierPhysicsPlugin)
            .add_plugin(ShapePlugin)
            .add_event::<Scored>()
            .add_event::<RestartGame>()
            .add_startup_system(setup.system())
            .add_startup_stage("screen_setup", SystemStage::single(world_borders.system()))
            .add_startup_stage_after("screen_setup", "team_setup", SystemStage::single(add_teams.system()))
            .add_startup_stage_after("team_setup", "game_setup", 
                SystemStage::parallel()
                    .with_system(add_goals.system())
                    .with_system(add_pucks.system())
                    .with_system(add_players.system())
            )
            .add_startup_system(setup_physics.system())
            .add_system(world_borders_changed.system())
            .add_system(keyboard_event_system.system().label("input"))
            .add_system(apply_control_force.system().after("input"))
            .add_system(update_scores.system())
            .add_system(score_colisions.system())
            .add_system(reset_game.system())
            .add_system(scored.system())
            .add_system(relocate_goals.system());
    }
}

fn setup(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn_bundle(UiCameraBundle::default());
}

fn setup_physics(mut config: ResMut<RapierConfiguration>) {
    config.gravity *= 0.0;
}

fn world_borders_changed(mut commands: Commands, mut window_events: EventReader<WindowResized>, query: Query<Entity, With<WorldBorder>>) {
    let window_event = match window_events.iter().last() {
        Some(event) => event,
        _ => return,
    };

    for entity in query.iter() {
        commands.entity(entity).despawn();
    }
    create_world_borders(commands, window_event.width, window_event.height);
}

fn create_world_borders(mut commands: Commands, width: f32, height: f32) {
    // top
    commands.spawn().insert(WorldBorder)
        .insert(RigidBodyBuilder::new_static().translation(0.0, height / 2.0))
        .insert(ColliderBuilder::halfspace(UnitVector2::new_normalize(Vector2::new(0.0, -1.0)))
            .friction(WALL_FRICTION)
            .restitution(WALL_RESTITUTION));
    // bottom
    commands.spawn().insert(WorldBorder)
        .insert(RigidBodyBuilder::new_static().translation(0.0, -height / 2.0))
        .insert(ColliderBuilder::halfspace(UnitVector2::new_normalize(Vector2::new(0.0, 1.0)))
            .friction(WALL_FRICTION)
            .restitution(WALL_RESTITUTION));
    // left
    commands.spawn().insert(WorldBorder)
        .insert(RigidBodyBuilder::new_static().translation(-width / 2.0, 0.0))
        .insert(ColliderBuilder::halfspace(UnitVector2::new_normalize(Vector2::new(1.0, 0.0)))
            .friction(WALL_FRICTION)
            .restitution(WALL_RESTITUTION));
    // right
    commands.spawn().insert(WorldBorder)
        .insert(RigidBodyBuilder::new_static().translation(width / 2.0, 0.0))
        .insert(ColliderBuilder::halfspace(UnitVector2::new_normalize(Vector2::new(-1.0, 0.0)))
            .friction(WALL_FRICTION)
            .restitution(WALL_RESTITUTION));
}

fn world_borders(commands: Commands, windows: Res<Windows>) {
    let window = windows.get_primary().expect("Missing game window");
    create_world_borders(commands, window.width(), window.height());
}

fn add_teams(mut commands: Commands, asset_server: Res<AssetServer>, mut color_materials: ResMut<Assets<ColorMaterial>>) {
    let text_alignments = vec![(JustifyContent::FlexEnd, AlignItems::FlexEnd), (JustifyContent::FlexStart, AlignItems::FlexEnd)];
    for (index, colour) in TEAM_COLOURS.iter().enumerate() {
        if index > text_alignments.len() {
            error!("Missing text alignment for teams");
            return;
        }
        create_team(&mut commands,
            *colour,
            color_materials.add(Color::NONE.into()),
            asset_server.load("fonts/FreeSans.ttf"),
            text_alignments[index].0,
            text_alignments[index].1,
        );
    }

    
}

fn add_goals(mut commands: Commands, team_colours: Query<(Entity, &Color), With<Team>>, windows: Res<Windows>) {
    let window = windows.get_primary().expect("Missing game window");
    let locations = vec![(-window.width() / 2.0, 0.0), (window.width() / 2.0, 0.0)];
    let rotation = vec![PI / 2.0, PI / 2.0];
    for (index, (entity, colour)) in team_colours.iter().enumerate() {
        if index > locations.len() {
            error!("Don't know where to put goals for all the teams!");
            break;
        }
        create_goal(&mut commands, entity, *colour, locations[index], rotation[index]);
    }
}

fn relocate_goals(
    goals: Query<Entity, With<Goal>>,
    mut window_events: EventReader<WindowResized>,
    mut rigid_body_handle_query: Query<&mut RigidBodyHandleComponent>,
    mut rigid_body_set: ResMut<RigidBodySet>,
) {
    if let Some(window_event) = window_events.iter().last() {
        let locations = vec![(-window_event.width / 2.0, 0.0), (window_event.width / 2.0, 0.0)];
        let rotation = vec![PI / 2.0, PI / 2.0];
        for (index, entity) in goals.iter().enumerate() {
            let rigid_body_handle = rigid_body_handle_query.get_component_mut::<RigidBodyHandleComponent>(entity)
                .expect("Entity with restart attribute is missing handle");
            
            if let Some(rigid_body) = rigid_body_set.get_mut(rigid_body_handle.handle()) {
                rigid_body.set_position(Isometry2::new(Vector2::new(locations[index].0, locations[index].1), rotation[index]), true);
            }
        }
    }
}

fn add_pucks(mut commands: Commands) {
    create_puck(&mut commands, Color::rgb(0.01, 0.01, 0.01), (0.0, 0.0));
    // create_puck(&mut commands, Color::BLACK, (0.0, -100.0));
}

fn add_players(mut commands: Commands) {
    create_player(
        &mut commands,
        TEAM_COLOURS[0],
        (-300.0, 100.0),
        Controls {up: KeyCode::W, down: KeyCode::S, left: KeyCode::A, right: KeyCode::D}
    );
    // create_player(
    //     &mut commands,
    //     TEAM_COLOURS[0],
    //     (-300.0, 0.0),
    //     Controls {up: KeyCode::T, down: KeyCode::G, left: KeyCode::F, right: KeyCode::H}
    // );
    create_player(
        &mut commands,
        TEAM_COLOURS[0],
        (-300.0, -100.0),
        Controls {up: KeyCode::I, down: KeyCode::K, left: KeyCode::J, right: KeyCode::L}
    );

    create_player(
        &mut commands,
        TEAM_COLOURS[1],
        (300.0, 100.0),
        Controls {up: KeyCode::Home, down: KeyCode::End, left: KeyCode::Delete, right: KeyCode::PageDown}
    );
    // create_player(
    //     &mut commands,
    //     TEAM_COLOURS[1],
    //     (300.0, 0.0),
    //     Controls {up: KeyCode::Up, down: KeyCode::Down, left: KeyCode::Left, right: KeyCode::Right}
    // );
    create_player(
        &mut commands,
        TEAM_COLOURS[1],
        (300.0, -100.0),
        Controls {up: KeyCode::Numpad8, down: KeyCode::Numpad5, left: KeyCode::Numpad4, right: KeyCode::Numpad6}
    );
}

fn scored(
    mut events: EventReader<Scored>,
    mut teams: Query<&mut Score, With<Team>>,
) {
    for event in events.iter() {
        let mut score = teams.get_component_mut::<Score>(event.0).expect("Team missing score component");
        score.0 += 1;
    }
}

fn reset_game(
    mut events: EventReader<RestartGame>,
    pucks: Query<(Entity, &StartPosition)>,
    mut rigid_body_handle_query: Query<&mut RigidBodyHandleComponent>,
    mut rigid_body_set: ResMut<RigidBodySet>,
) {
    if events.iter().last().is_none() {
        return;
    }
    for (entity, start_pos) in pucks.iter() {
        let rigid_body_handle = rigid_body_handle_query.get_component_mut::<RigidBodyHandleComponent>(entity)
            .expect("Entity with restart attribute is missing  handle");
        
        if let Some(rigid_body) = rigid_body_set.get_mut(rigid_body_handle.handle()) {
            rigid_body.set_linvel(Vector2::new(0.0, 0.0), true);
            rigid_body.set_position(Isometry2::new(Vector2::new(start_pos.0.0, start_pos.0.1), 0.0), true);
        }
    }
}

fn update_scores(
    teams: Query<(&Children, &Score), Changed<Score>>,
    mut score_texts: Query<&mut Text, With<ScoreText>>,
    mut event: EventWriter<RestartGame>,
) {
    for (children, score) in teams.iter() {
        let child = children.first().expect("Team has no score text");
        let mut score_text = score_texts.get_mut(*child).expect("Team score text missing component");

        let text = score_text.sections.get_mut(0).expect("Score text missing text section");
        text.value = format!("{}", score.0);
        
        event.send(RestartGame);
    }
}

fn create_team(commands: &mut Commands, colour: Color, colour_mat: Handle<ColorMaterial>, font: Handle<Font>, horizontal_align: JustifyContent, vertical_align: AlignItems) {
    commands.spawn()
    .insert(colour)
    .insert_bundle(TeamBundle {
        team: Team,
        score: Score(0),
    }).insert_bundle(NodeBundle {
        style: Style {
            position_type: PositionType::Absolute,
            position: Rect {
                left: Val::Px(10.0),
                top: Val::Px(10.0),
                right: Val::Px(10.0),
                bottom: Val::Px(10.0),
            },
            justify_content: horizontal_align,
            align_items: vertical_align,
            ..Default::default()
        },
        material: colour_mat,
        ..Default::default()
    }).with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![
                        TextSection {
                            value: "".to_string(),
                            style: TextStyle {
                                font_size: 40.0,
                                color: Color::rgb(0.6, 0.6, 0.6),
                                font,
                            }
                        }
                    ],
                    ..Default::default()
                },
                ..Default::default()
            }).insert(ScoreText);
        }
    );
}

fn create_goal(commands: &mut Commands, team: Entity, _colour: Color, location: (f32, f32), rotation: f32) {
    commands.spawn().insert(RigidBodyBuilder::new_static().translation(location.0, location.1).rotation(rotation))
    .insert(ColliderBuilder::cuboid(GOAL_WIDTH / 2.0, GOAL_DEPTH / 2.0).sensor(true))
    .insert_bundle(GeometryBuilder::build_as(
        &shapes::Rectangle {
            width: GOAL_WIDTH,
            height: GOAL_DEPTH,
            ..Default::default()
        },
        ShapeColors::outlined(Color::BLACK, Color::BLACK),
        DrawMode::Outlined {
            fill_options: FillOptions::default(),
            outline_options: StrokeOptions::default().with_line_width(2.0),
        },
        Transform::from_xyz(location.0, location.1, 10.0),
    )).insert(Goal { associated_team: team });
}

fn create_player(commands: &mut Commands, colour: Color, location: (f32, f32), controls: Controls) {
    commands.spawn().insert(RigidBodyBuilder::new_dynamic()
        .translation(location.0, location.1)
        .lock_rotations()
        .linear_damping(PLAYER_DAMPING)
    )
    .insert(ColliderBuilder::ball(PLAYER_RADIUS)
        .friction(PLAYER_FRICTION)
        .restitution(PLAYER_RESTITUTION)
    )
    .insert_bundle(GeometryBuilder::build_as(
        &shapes::Circle {
            radius: PLAYER_RADIUS,
            ..Default::default()
        },
        ShapeColors::outlined(colour, Color::BLACK),
        DrawMode::Outlined {
            fill_options: FillOptions::default(),
            outline_options: StrokeOptions::default().with_line_width(2.),
        },
        Transform::from_xyz(0.0, 0.0, 0.0),
    ))
    .insert_bundle(PlayerBundle {
        player: Player,
        control_force: ControlForce(PLAYER_FORCE),
        controls,
        controls_state: ControlsState(Vector2::new(0.0,0.0)),
    });
}

fn create_puck(commands: &mut Commands, colour: Color, location: (f32, f32)) -> Entity {
    commands.spawn().insert(RigidBodyBuilder::new_dynamic()
        .translation(location.0, location.1)
        .lock_rotations()
        .linear_damping(PUCK_DAMPING)
    )
    .insert(ColliderBuilder::ball(PUCK_RADIUS)
        .friction(PUCK_FRICTION)
        .restitution(PUCK_RESTITUTION)
    )
    .insert_bundle(GeometryBuilder::build_as(
        &shapes::Circle {
            radius: PUCK_RADIUS,
            ..Default::default()
        },
        ShapeColors::outlined(colour, Color::BLACK),
        DrawMode::Outlined {
            fill_options: FillOptions::default(),
            outline_options: StrokeOptions::default().with_line_width(0.0),
        },
        Transform::from_xyz(0.0, 0.0, 0.0),
    ))
    .insert(Puck)
    .insert(StartPosition(location)).id()
}

fn apply_control_force(
    time: Res<Time>,
    mut players: Query<(Entity, &ControlsState, &ControlForce), With<Player>>,
    mut rigid_body_handle_query: Query<&mut RigidBodyHandleComponent>,
    mut rigid_body_set: ResMut<RigidBodySet>,
) {
    for (entity, controls_state, control_force) in players.iter_mut() {
        let rigid_body_handle = rigid_body_handle_query.get_component_mut::<RigidBodyHandleComponent>(entity)
            .expect("Missing rigid body handler");
        if let Some(rigid_body) = rigid_body_set.get_mut(rigid_body_handle.handle()) {
            rigid_body.apply_impulse(Vector2::new(
                controls_state.0.x * control_force.0 * time.delta().as_secs_f32(),
                controls_state.0.y * control_force.0 * time.delta().as_secs_f32()), 
                true)
        }
    }
}

fn keyboard_event_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&mut ControlsState, &Controls), With<Player>>,
) {
    for (mut control_state, controls) in query.iter_mut() {
        control_state.0.x = 0.0;
        control_state.0.y = 0.0;
        if keyboard_input.pressed(controls.left) {
            control_state.0.x -= 1.0;
        }
        if keyboard_input.pressed(controls.right) {
            control_state.0.x += 1.0;
        }
        if keyboard_input.pressed(controls.up) {
            control_state.0.y += 1.0;
        }
        if keyboard_input.pressed(controls.down) {
            control_state.0.y -= 1.0;
        }
        control_state.0.try_normalize_mut(0.0);
    }

    if keyboard_input.pressed(KeyCode::F11) {
        // Fullscreen toggle
    }
}

fn score_colisions(
    events: Res<EventQueue>,
    mut score_event: EventWriter<Scored>,
    pucks: Query<&ColliderHandleComponent, With<Puck>>,
    goals: Query<(&ColliderHandleComponent, &Goal)>,
) {
    while let Ok(intersection_event) = events.intersection_events.pop() {
        if !intersection_event.intersecting {
            continue;
        }
        let target_goal = goals.iter().find_map(|c| {
            match c.0.handle() {
                h if h == intersection_event.collider2 => Some(c),
                h if h == intersection_event.collider1 => Some(c),
                _ => None,
            }
        });
        let target_puck = pucks.iter().find_map(|c| {
            match c.handle() {
                h if h == intersection_event.collider1 => Some(c),
                h if h == intersection_event.collider2 => Some(c),
                _ => None,
            }
        });
        let team_entity = match (target_puck, target_goal) {
            (Some(_), Some((_, goal))) => {
                // The Goal component contains a team entity
                goal.associated_team
            },
            _ => { continue }
        };
        
        score_event.send(Scored(team_entity));
    }
}
